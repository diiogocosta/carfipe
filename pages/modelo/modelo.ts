import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http'
import {AnoPage} from '../ano/ano';

/**
 * Generated class for the ModeloPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modelo',
  templateUrl: 'modelo.html',
})
export class ModeloPage {
  public modelos = [];  
  public marca = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http : Http) {
  }

  ionViewDidLoad() {        
    console.log(this.navParams.data);
    this.modelos = this.navParams.data.modelos;        
    this.marca = this.navParams.data.marca;
    console.log(this.marca);
  }

  modeloSelected(modelo){
    this.http.get('https://fipeapi.appspot.com/api/1/carros/veiculo/'+this.marca['id']+'/'+modelo.id+'.json').
      subscribe(response =>{
      this.navCtrl.push('AnoPage',{marca : this.marca,modelo : modelo, anos : response.json()});        
    });
  }

}
