import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Http } from '@angular/http'
import {DetalhesPage} from '../detalhes/detalhes';
/**
 * Generated class for the AnoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ano',
  templateUrl: 'ano.html',
})
export class AnoPage {
  public anos = [];
  public modelo = [];
  public marca = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http:Http ) {
  }

  ionViewDidLoad() {
    this.anos = this.navParams.data.anos;
    this.modelo = this.navParams.data.modelo;
    this.marca = this.navParams.data.marca;
  }

anoSelected(ano){
    this.http.get('https://fipeapi.appspot.com/api/1/carros/veiculo/'+this.marca['id']+'/'+this.modelo['id']+'/'+ano.fipe_codigo+'.json').
      subscribe(response =>{
      this.navCtrl.push('DetalhesPage',response.json());        
    });  
  

}

}
