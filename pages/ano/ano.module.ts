import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnoPage } from './ano';

@NgModule({
  declarations: [
    AnoPage,
  ],
  imports: [
    IonicPageModule.forChild(AnoPage),
  ],
})
export class AnoPageModule {}
