import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http } from '@angular/http'
import {ModeloPage} from '../modelo/modelo';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public marcas = [];    
  constructor(public navCtrl: NavController, public http: Http) {}

  ionViewDidLoad(){
    this.http.get('http://fipeapi.appspot.com/api/1/carros/marcas.json').subscribe(response => {
      this.marcas = response.json();          
    })
  }

  marcaSelected(marca){
    this.http.get('http://fipeapi.appspot.com/api/1/carros/veiculos/'+marca.id+'.json').subscribe(response =>{
      this.navCtrl.push('ModeloPage',{marca : marca, modelos : response.json()});

    })
  }
    

}


