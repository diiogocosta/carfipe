import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { Http } from '@angular/http'

/**
 * Generated class for the DetalhesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalhes',
  templateUrl: 'detalhes.html',
})
export class DetalhesPage {
  public anoModelo = [];
  public img : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http:Http, public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    let loading =  this.loadingCtrl.create({
        content: 'Aguarde, coletando dados...'         
      });      
    loading.present();     
    this.anoModelo = this.navParams.data;    
    this.http.get('https://www.googleapis.com/customsearch/v1?q='+this.anoModelo['name']+'&cx=006169841174321828973:lwutgnvvbyw&fileType=jpg&num=1&searchType=image&key=AIzaSyAPxNU8Yz-CkVZ7CRYkNR8lFve40n27CRs').
      subscribe(response =>{
      this.img = response.json().items[0].link;
      loading.dismiss;
    }),err => {loading.dismiss};          
  }

}
